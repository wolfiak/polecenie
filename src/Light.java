/**
 * Created by pacio on 19.02.2017.
 */
// Klasa odbiorcy
// model
public class Light {

    public void turnOn(){
        System.out.println("Swiatlo sie swieci");
    }

    public void turnOff(){
        System.out.println("Swiatlo sie nie swieci");
    }
}
