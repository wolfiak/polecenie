/**
 * Created by pacio on 19.02.2017.
 */
public class WylaczSwiatlo implements Polecenie{
    private Light swiatlo;

    public WylaczSwiatlo(Light swiatlo){
        this.swiatlo=swiatlo;
    }
    @Override
    public void wykonaj() {
        swiatlo.turnOff();
    }
}
