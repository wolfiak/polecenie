/**
 * Created by pacio on 19.02.2017.
 */
public class WlaczSwiatlo implements Polecenie{
    private Light swiatlo;

    public WlaczSwiatlo(Light swiatlo){
        this.swiatlo=swiatlo;
    }
    @Override
    public void wykonaj() {
        swiatlo.turnOn();
    }
}
